module gitlab.com/Tanya301/2048

go 1.14

require (
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	github.com/hajimehoshi/ebiten v1.11.3
	github.com/sirupsen/logrus v1.6.0
	golang.org/x/image v0.0.0-20200618115811-c13761719519
)
